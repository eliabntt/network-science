## Network analysis project ##

In this project I am going to analyze some characteristics of [en-Wikipedia]'s categorization system for Articles.

The subgraph I am starting from can be found [here](https://en.wikipedia.org/wiki/Category:Main_topic_classifications).
The idea for the dataset came from my Bachelor's thesis.

In the `HW1` folder you can find an analysis on degree distribution, clusterization, preferential attachment and some other metrics based on both categories and articles with their liks.
To run it you have to preprocess the whole database of categories and articles as it is expained in the `Introduction.pdf` file. After you have preprocessed the dataset you can easily export it in a `.csv` format and use that into the notebook.
With a simple flag you can analyze the graph as directed or undirected as you wish.

On the other hand, into `HW2` folder, I've tried to extract some more deep information like link prediction and page rank, but due to lack of computational power at the time I have limited myself only to the categories subset.
Due to the nature of the dataset this is not very meaningful. Maybe including the articles would have led to better and more meaningful results.


The whole project is done in `Python notebooks` and I have used `MySQL` to store the data.

### Dependencies
```
plotly
numpy
scipy
datetime
pandas
math
csv
matplotlib
```

----
(C) Elia Bonetto - 2018

[en-Wikipedia]:https://en.wikipedia.org/